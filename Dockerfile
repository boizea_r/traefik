FROM alpine:latest

ENV TIMEZONE Europe/Paris
ENV TRAEFIK_VERSION 1.6.2

WORKDIR /scripts

ADD https://github.com/containous/traefik/releases/download/v${TRAEFIK_VERSION}/traefik_linux-amd64 /usr/bin/traefik
RUN addgroup traefik && \
    adduser -s /bin/false -G traefik -S -D traefik

# Coping config & scripts
COPY ./config/ /etc/traefik/
COPY ./scripts ./

RUN chmod +x /usr/bin/traefik
RUN chmod +x ./start.sh

EXPOSE 80 443 8080

ENTRYPOINT [ "./start.sh" ]
